var service = analytics.getService('ren_chrome_plugin');
var tracker = service.getTracker('UA-5054140-13');

function existValue (val, data) {

    var result = false;
    $.each(data, function (id, value) {

        if (val == value) {

            result = true;

        }

    });

    return result;

}

function renImageLoad () {

    $('#ren-app .wrapper .right').each(function (id, data){

        var src = $(data).data('src');
        var url = $(data).parent().find('a').data('href');
        $(data).html('<i class="fa fa-4x fa-spinner fa-spin"></i>');

        if (src) {

            $(data).html('<a data-href="' + url + '" href="#' + url + '" title=""><img src="' + src + '" alt="Рен тв Новости"></a>');

        } else {

            $(data).html('<img src="/image/ren.png" alt="Нет картинки :(">');

        }

    });

}

function renSetContent (type) {

    $('#ren-app .wrapper').html('');
    var content = JSON.parse(localStorage.getItem('ren.content.' + type));
    var unread  = JSON.parse(localStorage.getItem('ren.unread'));
    unread      = unread != null ? unread : [];

    if (typeof content == 'object' && !$.isEmptyObject(content)) {

        var i = 0;
        $.each(content, function (id, row) {

            var bgcolor = 'tr-bg-white';
            if (i&1) {
                var bgcolor = 'tr-bg-gray';
            }

            if (row['video']) {
                row['title'] = row['title'] + ' <i class="fa fa-lg fa-play-circle a-color-orange" aria-hidden="true" title="Посмотреть видео"></i>';
            }
            $('#ren-app .wrapper').append(
                '<tr class="a-hbgcolor-gray a-transition" data-bgcolor="' + bgcolor + '" data-id="' + row['id'] + '">' +
                '<td class="left"><a href="#' + row["url"] + '" data-href="' + row["url"] + '" class="a-block" title="">' + row["title"] + '</a></td>' +
                '<td class="right" data-src="' + row['image'] + '" data-image_original="' + row['image_original'] + '"></td>' +
                '</tr>'
            );

            if (existValue(row['id'], unread)) {

                $('#ren-app .wrapper [data-id="' + row['id'] + '"]').addClass('unread');

            }
            i++;
        });

        renImageLoad();

    } else {

        $('#ren-app .wrapper').html('<div class="a-bgcolor-black" id="no-result"><img src="../image/travolta.gif" alt="Ничего нет :("></div>');

    }

    var cel =  localStorage.getItem('ren.weather.cel');
    if (cel && !$.cookie('ren.weather.logo')) {

        chrome.browserAction.setBadgeText({text: String(cel) + '°'});
        chrome.browserAction.setBadgeBackgroundColor({color:'#19E802'});

    } else {

        chrome.browserAction.setBadgeText({text: ''});

    }


    localStorage.setItem('ren.unread', JSON.stringify([]));

}

function renSearch (value) {

    $("#ren-app").scrollTop(0);
    tracker.sendEvent('Поиск', value);
    
    $('#ren-app .wrapper').html('');
    $('#ren-app #top').removeClass('active');
    $('#ren-app #last').removeClass('active');

    $.get('https://ren.tv/api/1/plugin/search/' + value, function(data) {

        if (typeof data.data == 'object' && !$.isEmptyObject(data.data)) {

            data = data.data;
            data = data.sort(function (a, b) {
                if (a.id < b.id) return 1;
                if (a.id > b.id) return -1;
            });

            var i = 0;
            $.each(data, function (id, row) {

                var bgcolor = 'a-bgcolor-white';
                if (i&1) {
                    var bgcolor = 'a-bgcolor-light-gray';
                }

                if (row['video']) {
                    row['title'] = row['title'] + ' <i class="fa fa-lg fa-play-circle a-color-orange" aria-hidden="true" title="Посмотреть видео"></i>';
                }
                $('#ren-app .wrapper').append(
                    '<tr class="' + bgcolor + ' a-hbgcolor-gray a-transition" data-id="' + row['id'] + '">' +
                    '<td class="left"><a href="#' + row["url"] + '" data-href="' + row["url"] + '" class="a-block" title="">' + row["title"] + '</a></td>' +
                    '<td class="right" data-src="' + row['image'] + '" data-image_original="' + row['image_original'] + '"></td>' +
                    '</tr>'
                );

                i++;

            });

            renImageLoad();

        } else {

            var map = {
                'q' : 'й', 'w' : 'ц', 'e' : 'у', 'r' : 'к',
                't' : 'е', 'y' : 'н', 'u' : 'г', 'i' : 'ш',
                'o' : 'щ', 'p' : 'з', '[' : 'х', ']' : 'ъ',
                'a' : 'ф', 's' : 'ы', 'd' : 'в', 'f' : 'а',
                'g' : 'п', 'h' : 'р', 'j' : 'о', 'k' : 'л',
                'l' : 'д', ';' : 'ж', '\'' : 'э', 'z' : 'я',
                'x' : 'ч', 'c' : 'с', 'v' : 'м', 'b' : 'и',
                'n' : 'т', 'm' : 'ь', ',' : 'б', '.' : 'ю',
                'Q' : 'Й', 'W' : 'Ц', 'E' : 'У', 'R' : 'К',
                'T' : 'Е', 'Y' : 'Н', 'U' : 'Г', 'I' : 'Ш',
                'O' : 'Щ', 'P' : 'З', 'A' : 'Ф', 'S' : 'Ы',
                'D' : 'В', 'F' : 'А', 'G' : 'П', 'H' : 'Р',
                'J' : 'О', 'K' : 'Л', 'L' : 'Д', 'Z' : '?',
                'X' : 'ч', 'C' : 'С', 'V' : 'М', 'B' : 'И',
                'N' : 'Т', 'M' : 'Ь', '`' : 'ё'
            };

            var r = '';
            for (var i = 0; i < value.length; i++) {
                r += map[value.charAt(i)] || value.charAt(i);
            }

            if (r.length && r != value) {

                renSearch(r);
                $('#ren-app #menu #search').val(r);

            } else {

                $('#ren-app .wrapper').html('<div class="a-bgcolor-black" id="no-result"><img src="../image/travolta.gif" alt="Ничего нет :("></div>');
                tracker.sendEvent('Поиск не дал результатов', value);

            }

        }

    }, "json");
    
    tracker.sendEvent('Запрос данных с ren.tv/api', 'search');

}

function renSetLast () {

    localStorage.setItem('ren.position', 'last');
    $('#ren-app .wrapper').html('');
    $('#ren-app #top').removeClass('active');
    $('#ren-app #last').addClass('active');
    $('#search').val('');
    renSetContent('last');
    $("#ren-app").scrollTop(localStorage.getItem('ren.scroll')|0);
    tracker.sendAppView('Просмотр последних новостей');

}

function renSetTop () {

    localStorage.setItem('ren.position', 'top');
    $('#ren-app .wrapper').html('');
    $('#ren-app #last').removeClass('active');
    $('#ren-app #top').addClass('active');
    $('#search').val('');
    renSetContent('top');
    $("#ren-app").scrollTop(localStorage.getItem('ren.scroll')|0);
    tracker.sendAppView('Просмотр популярных новостей');

}

function renSetCourse () {

    var euro = localStorage.getItem('ren.course.eur');
    if (euro != null) {

        $('#course').show();

    }

    var oldEuro = localStorage.getItem('ren.course.old.eur');
    $('#ren-app #euro span').text(euro);
    $('#ren-app #euro sup').html('');
    if (oldEuro && oldEuro != 'null' && oldEuro != euro) {

        if (euro < oldEuro) {
            $('#ren-app #euro sup').html('');
            $('#ren-app #euro sub').html('<i class="fa fa-long-arrow-down a-color-green"></i>');
        } else {
            $('#ren-app #euro sub').html('');
            $('#ren-app #euro sup').html('<i class="fa fa-long-arrow-up a-color-red"></i>');
        }

    }

    var usd    = localStorage.getItem('ren.course.usd');
    var oldUsd = localStorage.getItem('ren.course.old.usd');
    $('#ren-app #usd span').text(usd);
    $('#ren-app #usd sup').html('');
    if (oldUsd && oldUsd != 'null' && oldUsd != usd) {

        if (usd < oldUsd) {
            $('#ren-app #usd sup').html('');
            $('#ren-app #usd sub').html('<i class="fa fa-long-arrow-down a-color-green"></i>');
        } else {
            $('#ren-app #usd sub').html('');
            $('#ren-app #usd sup').html('<i class="fa fa-long-arrow-up a-color-red"></i>');
        }

    }

    var oil    = localStorage.getItem('ren.course.oil');
    var oldOil = localStorage.getItem('ren.course.old.oil');
    $('#ren-app #oil span').text(oil);
    $('#ren-app #oil sup').html('');
    if (oldOil && oldOil != 'null' && oldOil != oil) {

        if (oil < oldOil) {
            $('#ren-app #oil sup').html('');
            $('#ren-app #oil sub').html('<i class="fa fa-long-arrow-down a-color-red"></i>');
        } else {
            $('#ren-app #oil sub').html('');
            $('#ren-app #oil sup').html('<i class="fa fa-long-arrow-up a-color-green"></i>');
        }

    }

}

function renSetWeather () {

    var city = localStorage.getItem('ren.weather.city');
    var cel  = localStorage.getItem('ren.weather.cel');
    if (city != null) {

        $('#weather').show();

    }

    $('#ren-app #weather #city').text(city);
    $('#ren-app #weather #cel').text(cel);

}

$(window).load(function () {

    tracker.sendEvent('Открытие плагина (Нажатие на иконку)');

    $(window).scroll(function() {
        localStorage.setItem('ren.scroll', $(window).scrollTop());
    });

    renSetWeather();
    renSetCourse();

    var position = localStorage.getItem('ren.position');
    if (position == 'top') {

        renSetTop();

    } else {

        renSetLast();

    }

    $('#ren-app').on('click', '#last', function () {

        renSetLast();
        $("#ren-app").scrollTop(0);
        tracker.sendEvent('Нажатие на кнопку', 'Последнее');

    });

    $('#ren-app').on('click', '#top', function () {

        renSetTop();
        $("#ren-app").scrollTop(0);
        tracker.sendEvent('Нажатие на кнопку', 'Популярное');

    });

    $('#ren-app').on('click', '#usd', function () {

        var arr   = ["доллар","банк","фунт","юань","рубль","валюта","курс","америка","нефть","деньги","вашингтон","гамбургер","штат"];
        var rand  = Math.floor(Math.random() * arr.length);
        var value = arr[rand];
        $('#search').val(value);
        renSearch(value);
        tracker.sendEvent('Нажатие на доллар', value);

    });

    $('#ren-app').on('click', '#euro', function () {

        var arr   = ["евро","франция","германия","рим","геи","банк","фунт","юань","рубль","италия‎","валюта","курс","европа","запад","эмигрант"];
        var rand  = Math.floor(Math.random() * arr.length);
        var value = arr[rand];
        $('#search').val(value);
        renSearch(value);
        tracker.sendEvent('Нажатие на евро', value);

    });

    $('#ren-app').on('click', '#oil', function () {

        var arr   = ["нефть","газ","чёрное золото","бензин","топливо","скважина","газпром","ископаемые","рубль","доллар","авто","налог"];
        var rand  = Math.floor(Math.random() * arr.length);
        var value = arr[rand];
        $('#search').val(value);
        renSearch(value);
        tracker.sendEvent('Нажатие на нефть', value);

    });

    $('#ren-app').on('click', '#cel', function () {

        var arr   = ["погода", "ветер", "солнце", "дождь", "природа", "климат", "лето", "осадки", "циклон", "тепло", "холод", "лёд", "отпуск"];
        var rand  = Math.floor(Math.random() * arr.length);
        var value = arr[rand];
        $('#search').val(value);
        renSearch(value);
        tracker.sendEvent('Нажатие на температуру', value);

    });

    $('#ren-app').on('click', '#city', function () {

        var country = localStorage.getItem('ren.weather.country');
        var city = $(this).text();

        if (country) {

            var arr   = [country, city];
            var rand  = Math.floor(Math.random() * arr.length);
            var value = arr[rand];

        } else {

            var value = city;

        }

        $('#search').val(value);
        renSearch(value);
        tracker.sendEvent('Нажатие на город', value);

    });

    $('#ren-app #menu').on('keyup', '#search', function() {
        if (event.keyCode == 13) {

            var value = $(this).val();
            if (value.length >= 3) {
                renSearch(value);
            }

        }

    });

    $('#ren-app #menu').on('click', '#button-search', function() {

        var value = $('#search').val();
        if (value.length >= 3) {
            renSearch(value);
        }

    });

    var timerVideo = 0;
    $('#ren-app').on('mouseover', 'td i', function() {

        $(this).removeClass('fa-play-circle').addClass('fa-spinner').addClass('fa-spin');
        var self = $(this);
        timerVideo = setTimeout(function () {
            $('table.wrapper').hide();
            var id = self.parent().parent().parent().data('id');
            $('#ren-app #video #iframe-video').html('<iframe width="100%" height="466" src="https://ren.tv/player/' + id + '?autoplay=true" frameborder="0" allowfullscreen></iframe>');
            $('#ren-app #video').show(100);
            self.removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-play-circle');
            tracker.sendEvent('Просмотр видео', 'https://ren.tv/player/' + id + '?autoplay=true');
        }, 500);

    }).on('mouseout', 'td i', function() {
        clearTimeout(timerVideo);
        $(this).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-play-circle');
    });


    $('#ren-app').on('click', '#close-video', function () {

        $('table.wrapper').hide();
        $('#ren-app #video #iframe-video').html('');
        $('#ren-app #video').hide(100);

    });

    var timerLive = 0;
    $('#ren-app').on('mouseover', '#live i', function() {

        $(this).removeClass('fa-play').addClass('fa-spinner').addClass('fa-spin');
        var self = $(this);
        timerLive = setTimeout(function () {
            $('table.wrapper').hide();
            var id = self.parent().parent().parent().data('id');
            $('#ren-app #video #iframe-video').html('<iframe width="100%" height="466" src="https://ren.tv/player/live?autoplay=true" frameborder="0" allowfullscreen></iframe>');
            $('#ren-app #video').show(100);
            self.removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-play');
            tracker.sendEvent('Просмотр видео', 'https://ren.tv/player/live?autoplay=true');
        }, 500);

    }).on('mouseout', '#live i', function() {
        clearTimeout(timerLive);
        $(this).removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-play');
    });


    $('#ren-app').on('click', '#close-video', function () {

        $('table.wrapper').show();
        $('#ren-app #video #iframe-video').html('');
        $('#ren-app #video').hide(100);

    });

    var timerImage1 = 0;
    var timerImage2 = 0;
    $('#ren-app').on('mouseenter', '.right', function() {

        var self = $(this);
        $('#ren-app #image img').attr('src', self.data('image_original'));
        timerImage1 = setTimeout(function () {
            $('#ren-app #image #image-title').text(self.parent().find('a').text());
            $('#ren-app #image').show(100);
            $('#ren-app #image img').addClass('zoom');
            tracker.sendEvent('Просмотр картинки', self.data('image_original'));
        }, 500);
        timerImage2 = setTimeout(function () {
            $('#ren-app #image img').removeClass('zoom');
        }, 1500);

    }).on('mouseleave', '.right', function() {
        clearTimeout(timerImage1);
        clearTimeout(timerImage2);
        $('#ren-app #image img').removeClass('zoom');
        $('#ren-app #image').hide(50);
    });

    $('#ren-app').on('mouseover', 'tr', function () {

        $(this).removeClass('unread');

    });

    $('#ren-app header').on('click', 'a', function () {

        tracker.sendEvent('Переход на cайт', $(this).attr('href'));

    });

    $('#ren-app footer').on('click', 'a', function () {

        tracker.sendEvent('Переход на cайт', $(this).attr('href'));

    });

    $('#ren-app table').on('click', 'a', function () {

        tracker.sendEvent('Переход на новость', $(this).attr('href'));

    });

    $('#ren-app table').on('click', '[href]', function () {

        $('table.wrapper').hide();
        $('#ren-app #image #iframe-image').html('');
        $('#ren-app #image').hide(100);
        $('#ren-app #video #iframe-video').html('');
        $('#ren-app #vodeo').hide(100);

        var self = $(this);
        var id = self.parent().parent().data('id');
        $('#ren-app #post #iframe-post').html('<iframe src="https://ren.tv/post/' + id + '?title=true&description=true" frameborder="0" style="height:570px;width: 100%;display: block;" height="100%" width="100%"></iframe>');
        $('#ren-app #post').show(100);
        self.removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-play');
        tracker.sendEvent('Просмотр статьи', 'https://ren.tv/post/' + id);

    });

    $('#ren-app').on('click', '#close-post', function () {

        $('table.wrapper').show();
        $('#ren-app #post #iframe-post').html('');
        $('#ren-app #post').hide(100);

    });

    setInterval( function() {
        renSetWeather();
        renSetCourse();
    }, 1000*10);

});
