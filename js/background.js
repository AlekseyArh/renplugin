var service = analytics.getService('ren_chrome_plugin');
var tracker = service.getTracker('UA-5054140-13');
tracker.sendEvent('Запуск плагина (Срабатывает при запуске браузера)');

var maxPost = 40;

function existID (val, data) {

    var result = false;
    $.each(data, function (id, row) {

        if (row['id'] == val) {

            result = true;

        }

    });

    return result;

}

function renGetContent (type) {

    var content = JSON.parse(localStorage.getItem('ren.content.' + type));
    content     = content != null ? content : [];

    var unread = JSON.parse(localStorage.getItem('ren.unread'));
    unread     = unread != null ? unread : [];

    $.get('https://ren.tv/api/1/plugin/' + type, function (data) {

        tracker.sendEvent('Запрос данных с api.ren.tv', type);

        if (!$.isEmptyObject(data.data)) {

            data = data.data;
            $.each(data, function (id, row) {

                if (existID(row['id'], content) == false) {

                    content.push(row);

                    if (type == 'last') {
                        unread.push(row['id']);
                    }

                }

            });

            content = content.sort(function (a, b) {
                if (a.id < b.id) return 1;
                if (a.id > b.id) return -1;
            });
            content = content.slice(0, maxPost);

            if (type == 'last') {

                unread = unread.sort(function (a, b) {
                    if (a < b) return 1;
                    if (a > b) return -1;
                });
                unread  = unread.slice(0, maxPost);

                var count = unread.length;

                if (count > 0) {

                    chrome.browserAction.setBadgeText({text: String(count)});
                    chrome.browserAction.setBadgeBackgroundColor({color:'#E80235'});

                } else if ($.isEmptyObject(unread) && !$.cookie('ren.weather.logo')) {

                    var cel = localStorage.getItem('ren.weather.cel');
                    if (cel) {

                        chrome.browserAction.setBadgeText({text: String(cel) + '°'});
                        chrome.browserAction.setBadgeBackgroundColor({color:'#19E802'});

                    }

                } else {

                    chrome.browserAction.setBadgeText({text: ''});

                }

            }

            localStorage.setItem('ren.position', 'last');
            localStorage.setItem('ren.scroll', 0);
            localStorage.setItem('ren.unread', JSON.stringify(unread));
            localStorage.setItem('ren.content.' + type, JSON.stringify(content));

        }

    }, 'json');

}

function renGetCourse () {

    tracker.sendEvent('Запрос курсов');

    var euro = localStorage.getItem('ren.course.eur');
    localStorage.setItem('ren.course.old.eur', String(euro));

    $.get('http://www.micex.ru/issrpc/marketdata/currency/selt/daily/short/result.json?boardid=CETS&secid=EUR_RUB__TOM&lang=ru', function (data) {

        euro = data[1].LAST.toFixed(2);
        localStorage.setItem('ren.course.eur', String(euro));

    }, 'json');

    var usd = localStorage.getItem('ren.course.usd');
    localStorage.setItem('ren.course.old.usd', String(usd));

    $.get('http://www.micex.ru/issrpc/marketdata/currency/selt/daily/short/result.json?boardid=CETS&secid=USD000UTSTOM&lang=ru', function (data) {

        usd = data[1].LAST.toFixed(2);
        localStorage.setItem('ren.course.usd', String(usd));

    }, 'json');

    var oil = localStorage.getItem('ren.course.oil');
    localStorage.setItem('ren.course.old.oil', String(oil));

    $.get('https://meduza.io/api/v3/stock/all', function (data) {

        oil = data.brent.current.toFixed(2);
        localStorage.setItem('ren.course.oil', String(oil));

    }, 'json');

}

function renGetWeather () {

    tracker.sendEvent('Запрос погоды');

    var city    = localStorage.getItem('ren.weather.city');
    var cel     = localStorage.getItem('ren.weather.cel');
    var region  = localStorage.getItem('ren.weather.region');
    var country = localStorage.getItem('ren.weather.country');

    if (region && city) {

        $.ajax({
            type: 'GET',
            url: 'http://export.yandex.ru/bar/reginfo.xml?region=' + region,
            dataType: "xml",
            success: function (data) {

                var cel = $(data).find('info').find('weather').find('day').find('day_part').first().find('temperature').text();
                console.log(cel);
                if (cel) {
                    localStorage.setItem('ren.weather.cel', String(cel));
                }

            }
        });

    } else {

        $.ajax({
            type: "GET",
            url: "https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU",
            dataType: "text",
            success: function (data) {

                country = data.match('"country":"([a-zA-Zа-яА-Я- ]*)"');
                country = $.inArray(1, country) ? country['1'] : 'Россия';
                city = data.match('"region":"([a-zA-Zа-яА-Я- ]*)"');
                region = $.inArray('index', city) ? city['index'] : 0;
                city = $.inArray(1, city) ? city['1'] : 'Москва';

                localStorage.setItem('ren.weather.country', String(country));
                localStorage.setItem('ren.weather.city', String(city));
                localStorage.setItem('ren.weather.region', String(region));

                $.ajax({
                    type: "GET",
                    url: "http://export.yandex.ru/bar/reginfo.xml?region=" + region,
                    dataType: "xml",
                    success: function (data) {

                        var cel = $(data).find("info").find('weather').find('day').find('day_part').first().find('temperature').text();
                        if (cel) {

                            localStorage.setItem('ren.weather.cel', String(cel));

                        }

                    }
                });

            }
        });

    }

}

$(document).ready(function () {

    localStorage.removeItem('ren.content.last');
    localStorage.removeItem('ren.content.top');

    renGetWeather();
    renGetCourse();
    renGetContent('top');
    renGetContent('last');

    setInterval( function() {
        renGetContent('top');
        renGetContent('last');
    }, 1000*60*5);

    setInterval( function() {
        renGetWeather();
        renGetCourse();
    }, 1000*60*15);

});
