$(window).load(function () {

    var weather = $.cookie('ren.weather.logo');
    if (!weather) {
        $('#ren-app-option #weather').prop('checked', true);
    }

    var country = localStorage.getItem('ren.weather.country');
    var country   = $.cookie('ren.weather.country');
    country = country ? country : localStorage.getItem('ren.weather.country');
    if (country) {
        $('#ren-app-option #country').val(country);
    }

    var city   = $.cookie('ren.weather.city');
    city = city ? city : localStorage.getItem('ren.weather.city');
    if (city) {
        $('#ren-app-option #city').val(city);
    }

    var region   = $.cookie('ren.weather.region');
    region = region ? region : localStorage.getItem('ren.weather.region');
    if (region) {
        $('#ren-app-option #region').val(region);
    }

    $('#ren-app-option').on('click', '#weather', function (){

        var cel  = localStorage.getItem('ren.weather.cel');

        if ($(this).prop("checked")) {

            $.cookie('ren.weather.logo', null);

            if (cel) {

                chrome.browserAction.setBadgeText({text: String(cel) + '°'});
                chrome.browserAction.setBadgeBackgroundColor({color:'#19E802'});

            }

        } else {

            $.cookie('ren.weather.logo', true)
            chrome.browserAction.setBadgeText({text: ''});

        }

    });

});